	+ Miscellaneous Traversing:
		- .add() : dùng để thêm vào phần tử mới 1 tập hợp đã có sẵng.
		- .contents(): lấy ra cho chúng ta 1 tập hợp các phần tử bao gồm noteText, note phần tử và note Commands.
		- .each(): 
		- .end(): kết thúc 1 câu lệnh trên 1 dòng lệnh rất dài.
		
* Manipulation: Thao tác.
	+ Copying
		- .clone(): copy 1 đối tượng html nào đó.=>Có 2 tham số: True, False => Mặc định là False
			. True: thì copy luôn sự kiện của đối tượng. 
			. False: ko copy luôn sự kiện của đối tượng. 
	
	+ Around
		- .wrap(wrappingElement)
			. Tham số: wrappingElement => đối tượng HTML mà ta muốn bao bọc. => Thêm vào 1 thẻ mà chúng ta muốn bao bọc đối tượng chúng ta xác định
		- .wrap(function(index)): index => chính là thứ tự các phần tử con.
		- .wrapAll(wrappingElement) : tạo ra thẻ để bao bọc tập hợp đối tượng mà ta đã xác định.
		- .wrapInner(wrappingElement) => bao bọc lấy nội dung phía trong của phần tử ta chọn
	+ DOM Insertion, Inside: thêm 1 đối tượng nào đó vào trong 1 đối tượng khác.
		- .append():
			.append(content[,content])=> (có nghĩa là có thể đưa 1 lúc nhiều nội dung) : Thêm 1 đối tượng HTML nào đó vào trong đối tượng HTML mà ta đã xác định.
			.append(function(index,html)):
				++ Tham số html: chứa nội dung html của tập hợp
		- .appendTo(target):
			target: selector || Element || jQuery object.
		- .prepend(): thêm phần tử html vào đầu tập hợp.