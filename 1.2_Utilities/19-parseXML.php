<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Title of the document</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/xml.js"></script>
<script type="text/javascript">
	// Phương thức - parseHTML:
	var text;
	text = "<book><title>jQuery master</title><author>KhanhPham</author></book>";
	jQuery(document).ready(function(e) {
		
		$("#process").click(function(e) {
			var obj = $.parseXML(text);
			console.log(obj);
		});
	});
	
</script>
</head>
<body>
	<h1 style="text-align: center;">
		jQuery.parseHTML(data [ ,content ] [, keepScript]);</br>
		jQuery.parseHTML(data); => chuỗi html</br>
		jQuery.parseHTML(content); => nội dung html của 1 phần nào đó</br>
		jQuery.parseHTML(keepScript); => có giá trị true hoặc false</br>
	</h1>

	<div id="boxA" class="boxA">
		<div class="item">1.HTML</div>
		<div class="item">2.CSS</div>
		<div class="item">3.JS</div>
		<div class="item">4.jQuery</div>
	</div>

	<center>
		<input type="button" class="button" id="process" value="Process"/>
	</center>
</body>
</html>